package com.unit;


import com.vp.marktorecall.service.PasswordUtil;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class PasswordUtilTest {

    @Test
    public void shouldCreateSamePasswordHash() {
        String password = PasswordUtil.hashPassword("password");
        String anotherPassword = PasswordUtil.hashPassword("password");

        assertThat(password, is(anotherPassword));
    }
}
