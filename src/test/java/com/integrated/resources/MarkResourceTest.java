package com.integrated.resources;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.okhttp.*;
import com.vp.marktorecall.app.MarkToRecallApplication;
import com.vp.marktorecall.configuration.ServerConfiguration;
import com.vp.marktorecall.dto.MarkDTO;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.junit.ClassRule;
import org.junit.Test;

import java.io.IOException;

import static io.dropwizard.testing.ResourceHelpers.resourceFilePath;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class MarkResourceTest {

    private final OkHttpClient CLIENT = new OkHttpClient();

    public static final MediaType MEDIA_TYPE_APPLICATION_JSON
            = MediaType.parse("application/json; charset=utf-8");

    @ClassRule
    public static final DropwizardAppRule<ServerConfiguration> RULE =
            new DropwizardAppRule<ServerConfiguration>(MarkToRecallApplication.class, resourceFilePath("sample.yml"));

    @Test
    public void createMarkTest() throws IOException {
        Request request = new Request.Builder()
                .url(String.format("https://localhost:%d/mark/create?latitude=50.5&longitude=22.2&comment=sadssdjsdj", RULE.getPort(1)))
                .header("User-Agent", "OkHttp Headers.java")
                .addHeader("Authorization", "Basic ZWVldmlpQG1haWwucnU6cGFzc3dvcmQ=")
                .post(RequestBody.create(MEDIA_TYPE_APPLICATION_JSON, ""))
                .build();

        Call call = CLIENT.newCall(request);
        Response response = call.execute();
        if (response.isSuccessful()) {

            Headers responseHeaders = response.headers();
            for (int i = 0; i < responseHeaders.size(); i++) {
                System.out.println(responseHeaders.name(i) + ": " + responseHeaders.value(i));
            }

            ObjectMapper mapper = new ObjectMapper();
            MarkDTO markDTO = mapper.readValue(response.body().string(), MarkDTO.class);
            assertThat(markDTO.getComment(), is("sadssdjsdj"));
            assertThat(markDTO.getLatitude(), is(50.5));
            assertThat(markDTO.getLongitude(), is(22.2));
        }
    }
}
