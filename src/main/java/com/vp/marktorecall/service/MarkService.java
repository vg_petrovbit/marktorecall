package com.vp.marktorecall.service;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.vp.marktorecall.db.MarkDAO;
import com.vp.marktorecall.db.entities.Mark;
import com.vp.marktorecall.db.entities.User;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Singleton
public class MarkService {

    @Inject
    private MarkDAO markDAO;

    public MarkService() {
    }

    protected MarkDAO getMarkDAO() {
        return markDAO;
    }

    public Optional<Mark> create(Double latitude, Double longitude, String comment, User user) {
        Mark mark = new Mark();
        mark.setLatitude(latitude);
        mark.setLongitude(longitude);
        mark.setComment(comment);
        mark.setUser(user);
        mark.setUpdateDate(new Date());
        mark.setPublicId(UUID.randomUUID().toString());
        return getMarkDAO().create(mark);
    }

    public List<Mark> list(User user) {
        return getMarkDAO().list(user);
    }
}
