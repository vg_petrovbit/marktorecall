package com.vp.marktorecall.service;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.vp.marktorecall.db.UserDAO;
import com.vp.marktorecall.db.entities.User;

import java.util.Optional;

@Singleton
public class LoginService {

    @Inject
    private UserDAO userDAO;

    protected UserDAO getUserDAO() {
        return userDAO;
    }

    public Optional<User> login(String email, String password) {
        Optional<User> user = getUserDAO().getByEmail(email);
        if (user.isPresent()) {
            if (user.get().getPassword().equals(PasswordUtil.hashPassword(password))) {
                return user;
            }
        }
        return Optional.empty();
    }
}
