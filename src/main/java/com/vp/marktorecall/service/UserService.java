package com.vp.marktorecall.service;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.vp.marktorecall.db.UserDAO;
import com.vp.marktorecall.db.entities.User;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@Singleton
public class UserService {

    @Inject
    private UserDAO userDAO;

    public UserService() {
    }

    protected UserDAO getUserDAO() {
        return userDAO;
    }

    /**
     * Creates user
     *
     * @param email
     * @param name
     * @param password
     *
     * @return {@link User}
     */
    public Optional<User> create(String email, String name, String password) {
        User user = new User();
        user.setEmail(email);
        user.setName(name);
        user.setPassword(PasswordUtil.hashPassword(password));
        user.setPublicId(UUID.randomUUID().toString());
        user.setUpdateDate(new Date());

        return getUserDAO().create(user);
    }

    /**
     * Finds user by
     *
     * @param publicId
     *
     * @return {@link User}
     */
    public Optional<User> get(String publicId) {
        return getUserDAO().getByPubId(publicId);
    }
}
