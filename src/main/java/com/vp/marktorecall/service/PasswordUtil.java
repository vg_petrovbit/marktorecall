package com.vp.marktorecall.service;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

public final class PasswordUtil {

    // TODO: dynamic salt per user
    private final static String SALT = "super_secret_salt";
    public static final int ITERATIONS = 5;
    public static final int KEY_LENGTH = 256;

    protected static byte[] hashPassword(final char[] password, final byte[] salt, final int iterations, final int keyLength) {
        try {
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
            PBEKeySpec spec = new PBEKeySpec(password, salt,iterations, keyLength);
            SecretKey key = skf.generateSecret( spec );
            byte[] res = key.getEncoded();
            return res;
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e ) {
            throw new RuntimeException(e);
        }
    }

    public static String hashPassword(String password) {
        byte[] bytes = hashPassword(password.toCharArray(), SALT.getBytes(), ITERATIONS, KEY_LENGTH);
        return new String(bytes);
    }
}
