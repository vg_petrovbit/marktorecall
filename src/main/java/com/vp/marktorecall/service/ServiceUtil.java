package com.vp.marktorecall.service;

import com.google.inject.Injector;

public final class ServiceUtil {

    private static Injector injector;

    public static void setInjector(Injector injector) {
        ServiceUtil.injector = injector;
    }

    public static <T> T getInstance(Class<T> clazz) {
        return injector.getInstance(clazz);
    }
}
