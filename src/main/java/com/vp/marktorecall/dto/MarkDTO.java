package com.vp.marktorecall.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.vp.marktorecall.db.entities.Mark;

public class MarkDTO {

    private String publicId;
    private Double latitude;
    private Double longitude;
    private String comment;

    public MarkDTO() {
    }

    public MarkDTO(String publicId, Double latitude, Double longitude, String comment) {
        this.publicId = publicId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.comment = comment;
    }

    public static MarkDTO convertMark(Mark mark) {
        return new MarkDTO(mark.getPublicId(), mark.getLatitude(), mark.getLongitude(), mark.getComment());
    }

    @JsonProperty
    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    @JsonProperty
    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @JsonProperty
    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @JsonProperty
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
