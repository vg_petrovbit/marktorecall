package com.vp.marktorecall.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vp.marktorecall.db.entities.User;

public class UserDTO {

    private String email;
    private String name;

    public UserDTO() {
    }

    public UserDTO(String email, String name) {
        this.email = email;
    }

    public static UserDTO convertUser(User user) {
        return new UserDTO(user.getEmail(), user.getName());
    }

    @JsonProperty
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty
    public String getName() {
        return name;
    }

    public void setName(String firstName) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserDTO userDTO = (UserDTO) o;

        if (email != null ? !email.equals(userDTO.email) : userDTO.email != null) return false;
        return name != null ? name.equals(userDTO.name) : userDTO.name == null;
    }

    @Override
    public int hashCode() {
        int result = email != null ? email.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }
}
