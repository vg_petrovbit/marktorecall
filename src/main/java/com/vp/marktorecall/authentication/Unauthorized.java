package com.vp.marktorecall.authentication;

import io.dropwizard.auth.UnauthorizedHandler;

import javax.ws.rs.core.Response;

public class Unauthorized implements UnauthorizedHandler {
    @Override
    public Response buildResponse(String prefix, String realm) {
        return Response.status(Response.Status.UNAUTHORIZED)
                .build();
    }
}
