package com.vp.marktorecall.authentication;


import com.vp.marktorecall.db.entities.User;
import com.vp.marktorecall.service.LoginService;
import com.vp.marktorecall.service.ServiceUtil;
import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.auth.basic.BasicCredentials;

import java.util.Optional;


public class AuthenticatorService implements Authenticator<BasicCredentials, User> {

    private final LoginService loginService;

    public AuthenticatorService() {
        loginService = ServiceUtil.getInstance(LoginService.class);
    }

    @Override
    public Optional<User> authenticate(BasicCredentials credentials) throws AuthenticationException {
        Optional<User> user = loginService.login(credentials.getUsername(), credentials.getPassword());
        if (user.isPresent()) {
            return user;
        }
        return Optional.empty();
    }

    protected LoginService getLoginService() {
        return loginService;
    }
}
