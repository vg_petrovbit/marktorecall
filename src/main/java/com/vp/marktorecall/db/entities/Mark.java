package com.vp.marktorecall.db.entities;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "\"mark\"")
public class Mark {

    public static final String PUBLIC_ID = "public_id";
    public static final String USER_ID = "user_id";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String COMMENT = "comment";
    public static final String UPDATE_DATE = "update_date";

    private Long id;
    private String publicId;
    private User user;
    private Double latitude;
    private Double longitude;
    private String comment;
    private Date updateDate;


    public Mark() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "marks_seq_gen")
    @SequenceGenerator(name = "marks_seq_gen", sequenceName = "mark_id_seq")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = PUBLIC_ID)
    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = USER_ID, nullable = false)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Column(name = LATITUDE)
    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @Column(name = LONGITUDE)
    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Column(name = COMMENT)
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Column(name = UPDATE_DATE)
    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}
