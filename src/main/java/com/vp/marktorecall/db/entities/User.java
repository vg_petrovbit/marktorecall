package com.vp.marktorecall.db.entities;

import javax.persistence.*;
import java.security.Principal;
import java.util.Date;

@Entity
@Table(name = "\"user\"")
public class User implements Principal {

    public static final String PUBLIC_ID = "public_id";
    public static final String EMAIL = "email";
    public static final String NAME = "name";
    public static final String PASSWORD = "password";
    public static final String UPDATE_DATE = "update_date";

    private long id;
    private String publicId;
    private String email;
    private String name;
    private String password;
    private Date updateDate;

    public User() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_seq_gen")
    @SequenceGenerator(name = "users_seq_gen", sequenceName = "user_id_seq")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = PUBLIC_ID)
    public String getPublicId() {
        return publicId;
    }

    public void setPublicId(String publicId) {
        this.publicId = publicId;
    }

    @Column(name = EMAIL)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = NAME)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = PASSWORD)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = UPDATE_DATE)
    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (!publicId.equals(user.publicId)) return false;
        return email.equals(user.email);

    }

    @Override
    public int hashCode() {
        int result = publicId.hashCode();
        result = 31 * result + email.hashCode();
        return result;
    }
}
