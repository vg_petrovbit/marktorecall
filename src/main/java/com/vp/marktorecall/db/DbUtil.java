package com.vp.marktorecall.db;

import org.hibernate.SessionFactory;

public final class DbUtil {

    private static volatile SessionFactory sf;

    public static SessionFactory getSessionFactory() {
        return sf;
    }

    public static void setSessionFactory(SessionFactory sessionFactory) {
        sf = sessionFactory;
    }
}
