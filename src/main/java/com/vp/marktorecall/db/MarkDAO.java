package com.vp.marktorecall.db;

import com.google.inject.Singleton;
import com.vp.marktorecall.db.entities.Mark;
import com.vp.marktorecall.db.entities.User;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Singleton
public class MarkDAO {
    private static Logger LOGGER = Logger.getLogger(MarkDAO.class);

    private SessionFactory sessionFactory;

    public MarkDAO() {
        this.sessionFactory = DbUtil.getSessionFactory();
    }

    /**
     * Creates {@link Mark}
     *
     * @param mark
     * @return {@link Mark}
     */
    public Optional<Mark> create(Mark mark) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.save(mark);
            transaction.commit();
        } catch (Exception e) {
            LOGGER.fatal("Couldn't create mark ", e);
            transaction.rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
        return Optional.ofNullable(mark);
    }

    /**
     * Lists marks by
     *
     * @param user
     * @return List of {@link Mark}
     */
    public List<Mark> list(User user) {
        Session session = null;
        List<Mark> marks = Collections.emptyList();
        try {
            session = sessionFactory.openSession();
            marks = session.createQuery("from Mark where user=:user")
                    .setParameter("user", user)
                    .list();
        } catch (Exception e) {
            LOGGER.fatal("Couldn't create mark ", e);
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return marks;
    }
}