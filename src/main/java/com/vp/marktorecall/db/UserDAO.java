package com.vp.marktorecall.db;


import com.google.inject.Singleton;
import com.vp.marktorecall.db.entities.User;
import io.dropwizard.hibernate.AbstractDAO;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;

import java.util.Optional;

@Singleton
public class UserDAO {
    private SessionFactory sessionFactory;

    private static Logger LOGGER = Logger.getLogger(UserDAO.class);

    /**
     * Creates a new DAO
     *
     */
    public UserDAO() {
        this.sessionFactory = DbUtil.getSessionFactory();
    }

    /**
     * Inserts user to DB
     *
     * transaction inside
     *
     * @param user
     * @return {@link User}
     */
    public Optional<User> create(User user) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            ManagedSessionContext.bind(session);
            session.save(user);
            transaction.commit();
        } catch (Exception e) {
            LOGGER.fatal("Couldn't create user ", e);
            transaction.rollback();
        } finally {
            if (session != null) {
                session.close();
            }
            ManagedSessionContext.unbind(sessionFactory);
        }
        return Optional.ofNullable(user);
    }

    /**
     * Finds user by
     *
     * @param publicId
     *
     * @return {@link User}
     */
    public Optional<User> getByPubId(String publicId) {
        Session session = null;
        Transaction transaction = null;
        User result = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            result = (User) session.createQuery("from User where public_id=:pubId")
                    .setParameter("pubId", publicId)
                    .uniqueResult();
        } catch (Exception e) {
            transaction.rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return Optional.ofNullable(result);
    }

    /**
     * Finds user byw
     *
     * @param email
     * @return {@link User}
     */
    public Optional<User> getByEmail(String email) {
        Session session = null;
        Transaction transaction = null;
        User result = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            result = (User) session.createQuery("from User where email=:email")
                    .setParameter("email", email)
                    .uniqueResult();
        } catch (Exception e) {
            transaction.rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }

        return Optional.ofNullable(result);
    }
}
