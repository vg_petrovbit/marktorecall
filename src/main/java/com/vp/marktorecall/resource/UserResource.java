package com.vp.marktorecall.resource;

import com.google.inject.Inject;
import com.vp.marktorecall.db.entities.User;
import com.vp.marktorecall.dto.UserDTO;
import com.vp.marktorecall.service.ServiceUtil;
import com.vp.marktorecall.service.UserService;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.Optional;

@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {

    private final UserService userService;

    public UserResource() {
        this.userService = ServiceUtil.getInstance(UserService.class);
    }

    @POST
    @Path("/create")
    public UserDTO createUser(
            @QueryParam("name") String name,
            @QueryParam("email") String email,
            @QueryParam("password") String password
    ) {
        Optional<User> user = getUserService().create(email, name, password);
        return UserDTO.convertUser(user.get());
    }

    protected UserService getUserService() {
        return userService;
    }
}
