package com.vp.marktorecall.resource;

import com.google.inject.Inject;
import com.vp.marktorecall.db.entities.Mark;
import com.vp.marktorecall.db.entities.User;
import com.vp.marktorecall.dto.MarkDTO;
import com.vp.marktorecall.service.MarkService;
import com.vp.marktorecall.service.ServiceUtil;
import io.dropwizard.auth.Auth;

import javax.annotation.security.PermitAll;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Path("/mark")
@Produces(MediaType.APPLICATION_JSON)
public class MarkResource {

    private final MarkService markService;

    public MarkResource() {
        markService = ServiceUtil.getInstance(MarkService.class);
    }

    @PermitAll
    @POST
    @Path("/create")
    public MarkDTO createMark(
            @Auth User user,
            @QueryParam("latitude") Double latitude,
            @QueryParam("longitude") Double longitude,
            @QueryParam("comment") String comment
    ) {
        Optional<Mark> mark = getMarkService().create(latitude, longitude, comment, user);
        return MarkDTO.convertMark(mark.get());
    }

    @PermitAll
    @GET
    @Path("/list")
    public List<MarkDTO> listMarks(
            @Auth User user
    ) {
        List<Mark> list = getMarkService().list(user);
        return list.stream().map(mark -> MarkDTO.convertMark(mark)).collect(Collectors.toList());
    }

    protected MarkService getMarkService() {
        return markService;
    }
}
