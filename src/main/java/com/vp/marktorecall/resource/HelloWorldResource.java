package com.vp.marktorecall.resource;

import com.google.inject.Inject;
import com.vp.marktorecall.dto.Saying;
import com.vp.marktorecall.service.ServiceUtil;
import com.vp.marktorecall.service.UserService;

import javax.annotation.security.PermitAll;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;


@Deprecated
@Path("/hello-world")
@Produces(MediaType.APPLICATION_JSON)
public class HelloWorldResource {
    private final String template;
    private final String defaultName;
    private final AtomicLong counter;

    private UserService userService;

    public HelloWorldResource(String template, String defaultName) {
        this.template = template;
        this.defaultName = defaultName;
        this.counter = new AtomicLong();
        this.userService = ServiceUtil.getInstance(UserService.class);
    }

    @GET
    public Saying sayHello(@QueryParam("name") Optional<String> name) {
//        getUserService().create(name.orElse(defaultName), UUID.randomUUID().toString(), "secret");

        final String value = String.format(template, name.orElse(defaultName));
        return new Saying(counter.incrementAndGet(), value);
    }

    public UserService getUserService() {
        return userService;
    }
}
