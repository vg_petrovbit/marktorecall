package com.vp.marktorecall.app;


import com.codahale.metrics.ConsoleReporter;
import com.codahale.metrics.MetricRegistry;

import java.util.concurrent.TimeUnit;

public class Metrics {

    private static final MetricRegistry metrics = new MetricRegistry();

    public static MetricRegistry getMetrics() {
        return metrics;
    }

    public static void startReport() {
        ConsoleReporter reporter = ConsoleReporter.forRegistry(metrics)
                .convertRatesTo(TimeUnit.SECONDS)
                .convertDurationsTo(TimeUnit.MILLISECONDS)
                .build();
        reporter.start(20, TimeUnit.SECONDS);
    }
}
