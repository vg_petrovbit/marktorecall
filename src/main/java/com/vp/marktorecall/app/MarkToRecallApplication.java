package com.vp.marktorecall.app;


import com.google.inject.Guice;
import com.vp.marktorecall.authentication.AuthenticatorService;
import com.vp.marktorecall.authentication.AuthorizerService;
import com.vp.marktorecall.authentication.Unauthorized;
import com.vp.marktorecall.configuration.ServerConfiguration;
import com.vp.marktorecall.configuration.ServiceModule;
import com.vp.marktorecall.db.DbUtil;
import com.vp.marktorecall.db.entities.Mark;
import com.vp.marktorecall.db.entities.User;
import com.vp.marktorecall.resource.HelloWorldResource;
import com.vp.marktorecall.resource.MarkResource;
import com.vp.marktorecall.resource.UserResource;
import com.vp.marktorecall.service.ServiceUtil;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.auth.AuthDynamicFeature;
import io.dropwizard.auth.AuthValueFactoryProvider;
import io.dropwizard.auth.basic.BasicCredentialAuthFilter;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

public class MarkToRecallApplication extends Application<ServerConfiguration> {

    public static void main(String[] args) throws Exception {
        new Metrics();
        new MarkToRecallApplication().run(args);
    }

    // Add Hibernate Entities here
    private final HibernateBundle<ServerConfiguration> hibernate = new HibernateBundle<ServerConfiguration>(
            User.class,
            Mark.class
    ) {

        @Override
        public DataSourceFactory getDataSourceFactory(ServerConfiguration configuration) {
            return configuration.getDataSourceFactory();
        }
    };

    @Override
    public String getName() {
        return "hello-world";
    }

    @Override
    public void initialize(Bootstrap<ServerConfiguration> bootstrap) {
        bootstrap.addBundle(hibernate);

        // TODO: delete
        bootstrap.addBundle(new AssetsBundle("/assets/css", "/css", null, "css"));
    }

    @Override
    public void run(ServerConfiguration configuration, Environment environment) throws Exception {
        DbUtil.setSessionFactory(hibernate.getSessionFactory());
        ServiceUtil.setInjector(Guice.createInjector(new ServiceModule()));


        // TODO: automatic resource create
        final HelloWorldResource resource = new HelloWorldResource(
                configuration.getTemplate(),
                configuration.getDefaultName()
        );

        // Authorization by token
        // https://spin.atomicobject.com/2016/07/26/dropwizard-dive-part-1/
        enableBasicAuthentication(environment);

        environment.jersey().register(resource);
        environment.jersey().register(new UserResource());
        environment.jersey().register(new MarkResource());
    }


    /**
     * Enables basic com.vp.marktorecall.authentication
     *
     * @param environment
     */
    public void enableBasicAuthentication(Environment environment) {
        environment.jersey().register(
                new AuthDynamicFeature(
                    new BasicCredentialAuthFilter.Builder<User>()
                        .setAuthenticator(new AuthenticatorService())
                        .setAuthorizer(new AuthorizerService())
                        .setRealm("SUPER SECRET STUFF")
                        .setUnauthorizedHandler(new Unauthorized())
                        .buildAuthFilter()
                )
        );
        environment.jersey().register(RolesAllowedDynamicFeature.class);
        // Added to use @Auth annotation in Resources
        environment.jersey().register(new AuthValueFactoryProvider.Binder<>(User.class));
    }
}
