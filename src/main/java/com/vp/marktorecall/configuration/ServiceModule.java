package com.vp.marktorecall.configuration;

import com.google.inject.AbstractModule;
import com.vp.marktorecall.db.MarkDAO;
import com.vp.marktorecall.db.UserDAO;

public class ServiceModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(UserDAO.class);
        bind(MarkDAO.class);
    }
}
