CREATE TABLE "user"
(
  id BIGSERIAL PRIMARY KEY NOT NULL,
  public_id VARCHAR(254) NOT NULL,
  email VARCHAR(254) NOT NULL,
  name TEXT NOT NULL,
  password TEXT,
  update_date TIMESTAMP WITH TIME ZONE NOT NULL
);
CREATE UNIQUE INDEX user_id_uindex ON public."user" (id);
CREATE UNIQUE INDEX user_public_id_uindex ON public."user" (public_id);
CREATE UNIQUE INDEX user_email_uindex ON public."user" (email);
COMMENT ON TABLE public."user" IS 'User table';

ALTER SEQUENCE user_id_seq RESTART WITH 1;



-- 56.8418084,53.2259724
CREATE TABLE mark (
  id BIGSERIAL NOT NULL,
  public_id VARCHAR(254) NOT NULL,
  user_id BIGINT NOT NULL,
  latitude DOUBLE PRECISION,
  longitude DOUBLE PRECISION,
  comment TEXT,
  update_date TIMESTAMP WITH TIME ZONE NOT NULL
);
CREATE UNIQUE INDEX mark_id_uindex ON mark USING BTREE (id);
CREATE UNIQUE INDEX mark_public_id_uindex ON mark USING BTREE (public_id);
COMMENT ON TABLE public.mark IS 'marks on map';

ALTER SEQUENCE mark_id_seq RESTART WITH 1;